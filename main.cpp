#include <iostream>
#include <vector>
#include <math.h>
#include <SFML/Graphics.hpp>

using std::vector;

const unsigned framerateLimit = 60;

struct Enemy{
	double x,y;
	sf::Texture txt;
	sf::Sprite spr;

	Enemy(){
		if (!txt.loadFromFile("enemy.png")){
			std::cerr << "Could not open enemy.png\n";
			return;
		}

		spr.setTexture(txt);
	}

	unsigned get_width(){return spr.getGlobalBounds().width;}
	unsigned get_height(){return spr.getGlobalBounds().height;}

	void set_position(const double newX, const double newY){
		x=newX; y=newY;
		spr.setPosition(x,y);
	}

	void draw(sf::RenderWindow &window){
		window.draw(spr);
	}
};

class Player{
	friend class World;

	double x=0,y=0;
	double lastX=0,lastY=0;
	double speed=8;
	bool jumping=false;
	double jumpHeight=100; // In pixels
	double jumpTimeToApex=0.5; // Seconds
	double jumpGravity;
	double initialJumpVelocity;
	double jumpVelocity;
	double jumpHeightGain=0;
	sf::Texture txt;
	sf::Sprite spr;
	public:

	Player(){
		if (!txt.loadFromFile("player.png")){
			std::cerr << "Could not open player.png\n";
			return;
		}

		spr.setTexture(txt);
	}

	unsigned get_width(){return spr.getGlobalBounds().width;}
	unsigned get_height(){return spr.getGlobalBounds().height;}

	void set_position(const double newX, const double newY){
		lastX=x; lastY=y;
		x=newX; y=newY;
		spr.setPosition(x,y);
	}

	void update(){
		lastX = x; lastY = y - jumpHeightGain;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			x -= speed;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			x += speed;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
			// Start of jump
			if (!jumping){
				jumpGravity = (2*jumpHeight) / (jumpTimeToApex*jumpTimeToApex);
				initialJumpVelocity = jumpGravity * jumpTimeToApex;
				jumpVelocity = initialJumpVelocity;
			}

			jumping = true;
		}

		if (jumping)
			jumpVelocity -= initialJumpVelocity;

		if (jumpHeightGain < 0){
			jumping = false;
			jumpHeightGain=0;
		}

		spr.setPosition(x, y - jumpHeightGain);
	}

	void draw(sf::RenderWindow &window){
		window.draw(spr);
	}
};

class World{
	public:

	vector <Player> players;
	vector <Enemy> enemies;

	void add_player(Player player){
		players.push_back(player);
	}

	void add_enemy(Enemy enemy){
		enemies.push_back(enemy);
	}

	void update(){
		if (enemies.empty())
			return;

		for (Player &p : players){
			for (unsigned enemyIdx = 0; enemyIdx < enemies.size(); enemyIdx++){
				// Player jumped on enemy (player inside enemy, last Y position was above enemy)
				if (p.spr.getGlobalBounds().intersects(enemies[enemyIdx].spr.getGlobalBounds()) && (p.lastY + p.get_height()) < enemies[enemyIdx].spr.getGlobalBounds().top)
					enemies.erase(enemies.begin() + enemyIdx);
			}
		}
	}

	void draw(sf::RenderWindow &window){
		for (Player &p : players)
			p.draw(window);
		for (Enemy &e : enemies)
			e.draw(window);
	}
};

int main(){
	sf::RenderWindow window(sf::VideoMode(1280,720), "Game", sf::Style::Titlebar | sf::Style::Close);
	window.setFramerateLimit(framerateLimit);

	sf::View view;
	view.reset(sf::FloatRect(0,0, window.getSize().x, window.getSize().y));
	view.setViewport(sf::FloatRect(0,0, 1,1));
	view.zoom(1/2.0);

	Player player;
	player.set_position(window.getSize().x/2, window.getSize().y - player.get_height());

//	Enemy enemy;
//	enemy.set_position(window.getSize().x/2 + 100, window.getSize().y - enemy.get_height());

	World world;
	world.add_player(player);
//	world.add_enemy(enemy);

//	enemy.set_position(window.getSize().x/2 + 250, window.getSize().y - enemy.get_height());

//	world.add_enemy(enemy);

	const unsigned playerHeight = world.players[0].get_height();
	const unsigned playerWidth = world.players[0].get_width();

	while (window.isOpen()){
		sf::Event e;
		while (window.pollEvent(e)){
			if (e.type == sf::Event::Closed)
				window.close();
		}

		world.players[0].update();
		world.update();

//		view.setCenter(world.players[0].spr.getPosition().x - playerWidth/2, window.getSize().y - world.players[0].get_height());
//		window.setView(view);

		window.clear();

		world.draw(window);

		window.display();
	}
}
